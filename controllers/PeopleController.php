<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\People;

class PeopleController extends Controller{
    public function actionDb(){

        $query = People::find();
        $count = $query->count();

        $pagination = new Pagination([
            'defaultPageSize' => 3,
            'totalCount' => $query->count(),
        ]);

        $result = $query->orderBy('ID')
                        ->offset($pagination->offset)
                        ->limit($pagination->limit)
                        ->all();

        return $this->render("dbconn",[
            'count' => $count,
            'result' => $result,
            'pagination' => $pagination,
        ]);
    }
}
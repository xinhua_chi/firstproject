<?php

namespace app\controllers;

use app\models\BoardForm;
use app\models\PublishForm;
use Yii;
use yii\web\Controller;
use app\models\Userlogin;
use app\models\Article;
use app\models\Reply;
use yii\data\Pagination;

class WelcomeController extends Controller{

    public function actionLogin(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Userlogin();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionBoard(){

        $model = new BoardForm();

        $query = Article::find();
        $sub_query = Reply::find();

        $pagination = new Pagination([
            'defaultPageSize' => 2,
            'totalCount' => $query->count(),
        ]);

        $result = $query->orderBy('ID')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $sub_result = $sub_query->all();

        return $this->render("reply",[
            'model' => $model,
            'result' => $result,
            'sub_result' => $sub_result,
            'pagination' => $pagination,
        ]);
    }

    public function actionPublish(){
        $model = new PublishForm();
        return $this->render("publish",[
            'model' => $model,
        ]);
    }
}
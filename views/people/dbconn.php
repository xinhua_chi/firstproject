<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<?php foreach($result as $item): ?>

<li>
    <?= Html::encode("ID : $item->ID / Name : $item->Name / Num : $item->Num") ?>
</li>

<?php endforeach; ?>
<?= LinkPager::widget(['pagination' => $pagination]); ?>

<head>
    <link rel="stylesheet" href="/css/content.css">
</head>
<body>
<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;

$this->title = '貼文版';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php foreach($result as $item): ?>
    <?php $form = ActiveForm::begin([
        'id' => 'contact-form',
        'fieldConfig' => [
            'template' => "<div class='reply'>{input}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    <div class='article'>
        <?= Html::encode("#$item->ID") ?>
        <br><br>
        <?= Html::encode("$item->user :") ?>
        <br><br>
        <div class='content'>
            <?= Html::encode("$item->content") ?>
        </div>

        <?php foreach($sub_result as $detail): ?>
            <?php if($detail->A_ID == $item->ID): ?>
                <div class="reply">
                    <?= Html::encode("$detail->replier : $detail->response"); ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>


        <?= $form->field($model, 'content')->textarea() ?>
        <?= Html::submitButton('回覆', ['class' => 'btn btn-primary',]) ?>

    </div>
    <?php ActiveForm::end() ?>
<?php endforeach; ?>
<?= LinkPager::widget(['pagination' => $pagination]); ?>

</body>

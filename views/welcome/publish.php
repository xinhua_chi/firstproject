<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '發文';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model,"content")->textarea(["row" => 6]) ?>
<?php ActiveForm::end(); ?>


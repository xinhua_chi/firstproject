<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PublishForm extends Model
{
    public $user;
    public $content;

    public function rules()
    {
        return [
            ['content', 'required'],
        ];
    }
}